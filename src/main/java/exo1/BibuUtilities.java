package exo1;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;

public class BibuUtilities {
	
	Bibliothèque bib;
	GlobalBibliographyAccess glBib;
	Clock horloge;
	
	public BibuUtilities(Bibliothèque bib,GlobalBibliographyAccess glBib) {
		this.bib=bib;
		this.glBib=glBib;
		horloge=Clock.systemDefaultZone();
	}
	
	public ArrayList<NoticeBibliographique> chercherNoticesConnexes(NoticeBibliographique ref) {
		ArrayList<NoticeBibliographique> res = new ArrayList<NoticeBibliographique>();
		Integer count = 0;
		ArrayList<NoticeBibliographique> nBaut= glBib.noticesDuMemeAuteurQue(ref);
		for(NoticeBibliographique n : nBaut) {
			if (ref.getTitre()!=n.getTitre()) {
				boolean temp = true;
				for (NoticeBibliographique r:res) {
					if (r.getTitre()==n.getTitre()) {
						temp=false;
					}
				}
				if (temp){
					res.add(n);
					count++;
					if (count==5)
						return res;
				}
			}
		}
		
				
		return res;
	}
	
	public NoticeStatus ajoutNotice(String isbn) throws AjoutImpossibleException, IncorrectIsbnException {
		
		NoticeBibliographique not;
		not = glBib.getNoticeFromIsbn(isbn);

		NoticeStatus stat =bib.addNotice(not);
		if(stat == NoticeStatus.notExisting) {
			throw new AjoutImpossibleException();
		}
		return stat;
	}
	
	public boolean prévoirInventaire(){
		Instant now =horloge.instant();
		Instant dLI =bib.getLastInventaire().plusMonths(12).atStartOfDay(ZoneId.systemDefault()).toInstant();
		return now.isAfter(dLI);
	
	}

}
