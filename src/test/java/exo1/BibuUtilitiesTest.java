package exo1;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class BibuUtilitiesTest{
	
	private Bibliothèque bib;
	private GlobalBibliographyAccess glBib;


	@Mock
	private Clock mockclk;
	
	@InjectMocks 
	private BibuUtilities bibUt;

	

	@BeforeEach
	private void init(){
		bib= Bibliothèque.getInstance();
		glBib= mock(GlobalBibliographyAccess.class);
		bibUt= new BibuUtilities(bib,glBib);
	}
	
	private static Stream<Arguments> sameAutor(){

		NoticeBibliographique sameTit= new NoticeBibliographique("2", "salut", "mr");
		NoticeBibliographique not1= new NoticeBibliographique("3", "salut2", "mr");
		NoticeBibliographique not2= new NoticeBibliographique("4", "salut3", "mr");
		NoticeBibliographique not3= new NoticeBibliographique("5", "salut4", "mr");
		NoticeBibliographique not4= new NoticeBibliographique("6", "salut5", "mr");
		NoticeBibliographique not5= new NoticeBibliographique("7", "salut6", "mr");
		NoticeBibliographique not6= new NoticeBibliographique("8", "salut7", "mr");
		NoticeBibliographique dup= new NoticeBibliographique("13", "salut2", "mr");

		ArrayList<NoticeBibliographique> data1 = new ArrayList<NoticeBibliographique>();

		ArrayList<NoticeBibliographique> data2 = new ArrayList<NoticeBibliographique>();
		data2.add(sameTit);

		ArrayList<NoticeBibliographique> data3 = new ArrayList<NoticeBibliographique>();
		data3.add(not1);

		ArrayList<NoticeBibliographique> data4 = new ArrayList<NoticeBibliographique>();
		data4.add(not1);
		data4.add(not2);
		data4.add(not3);
		data4.add(not4);
		data4.add(not5);

		ArrayList<NoticeBibliographique> data5 = new ArrayList<NoticeBibliographique>();
		data5.add(not1);
		data5.add(not2);
		data5.add(not3);
		data5.add(not4);
		data5.add(not5);
		data5.add(not6);

		ArrayList<NoticeBibliographique> data6 = new ArrayList<NoticeBibliographique>();
		data6.add(not1);
		data6.add(not2);
		data6.add(not3);
		data6.add(not4);
		data6.add(dup);

		return Stream.of(
			Arguments.of(data1,0,"0"),
			Arguments.of(data2,0,"1 same title than ref"),
			Arguments.of(data3,1,"1"),
			Arguments.of(data4,5,"5"),
			Arguments.of(data5,5,"more than 5"),
			Arguments.of(data6,4,"5 with 1 dup")
		);
	}
	@ParameterizedTest(name="test :{index} - {2}")
	@MethodSource("sameAutor")
	public void chercherNoticesConnexesT(ArrayList<NoticeBibliographique> data, int res, String name) {
		//given
		
		NoticeBibliographique not = new NoticeBibliographique("1", "salut", "mr");
		ArrayList<NoticeBibliographique> notco;
		
		//when
		when(glBib.noticesDuMemeAuteurQue(not)).thenReturn(data);

		//then
		notco= bibUt.chercherNoticesConnexes(not);
		assertEquals(res, notco.size());
		
	}

	private static Stream<Arguments> aNotice(){
		NoticeBibliographique inIsbn = new NoticeBibliographique("-1", "salut", "mr");
		NoticeBibliographique nullnot = null;
		NoticeBibliographique newA= new NoticeBibliographique("2", "salut", "mr");
		NoticeBibliographique noC= new NoticeBibliographique("1", "salut", "mr");
		NoticeBibliographique upd=  new NoticeBibliographique("1", "salut2", "mr");

		return Stream.of(
			Arguments.of("-1",inIsbn,null,"incorect Isbn"),
			Arguments.of("34",nullnot,null,"Ajout impossible"),
			Arguments.of("2",newA,NoticeStatus.newlyAdded,"newlyAdded"),
			Arguments.of("1",noC,NoticeStatus.nochange,"noChange"),
			Arguments.of("1",upd,NoticeStatus.updated,"updated")

			);
	}
	@ParameterizedTest(name = "test : {index} - {3}")
	@MethodSource("aNotice")
	public void ajoutNoticeT(String isbn,NoticeBibliographique not,NoticeStatus res, String name) throws IncorrectIsbnException {
		//given
		NoticeBibliographique n= new NoticeBibliographique("1", "salut", "mr");
		bib.addNotice(n);
		NoticeStatus stat;

		//when
		if(isbn != "-1"){
		
			when(glBib.getNoticeFromIsbn(isbn)).thenReturn(not);
			
		}
		if(isbn== "-1"){
			when(glBib.getNoticeFromIsbn(isbn)).thenThrow(new IncorrectIsbnException());
		}

		//then
		try {
			stat=bibUt.ajoutNotice(isbn);
		} catch (AjoutImpossibleException e) {
			assertNull(not);
			return;
		} catch(IncorrectIsbnException e){
			assertEquals(isbn, "-1");
			return;
		}

		assertEquals(res, stat);

	}

	private static Stream<Arguments> pInventaire(){
		LocalDate no=LocalDate.now(Clock.systemDefaultZone()).plusMonths(6);
		LocalDate no2=LocalDate.now(Clock.systemDefaultZone()).plusMonths(12);
		LocalDate yes=LocalDate.now(Clock.systemDefaultZone()).plusMonths(13);
		return Stream.of(
			Arguments.of(no,false,"less 1y"),
			Arguments.of(no2,false,"exact 1y"),
			Arguments.of(yes,true,"more than 1y")
		);
	}

	@ParameterizedTest(name="test :{index} - {2}")
	@MethodSource("pInventaire")
	public void prévoirInventaireT(LocalDate mockLD ,boolean expected,String name){

		//given
		MockitoAnnotations.openMocks(this);
		bib.inventaire();

		Clock fixed=Clock.fixed(mockLD.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());

		//when
		when(mockclk.instant()).thenReturn(fixed.instant());

		//then
		boolean res=bibUt.prévoirInventaire();
		assertEquals(res, expected);
	}
	
}